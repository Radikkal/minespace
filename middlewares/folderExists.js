'use strict';

const getDB = require('../bbdd/getConnection');
const { generateError } = require('../helpers');

const folderExists = async (req, res, next) => {
    let connection;

    try {
        connection = await getDB();

        const { idFolder } = req.params;

        const [folder] = await connection.query(
            `
            SELECT id FROM folders WHERE id=?
            `,
            [idFolder]
        );

        if (folder.length === 0) {
            generateError('No existe la carpeta', 404);
        }

        next();
    } catch (err) {
        next(err);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = folderExists;
