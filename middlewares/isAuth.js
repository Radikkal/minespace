'use strict';
const jwt = require('jsonwebtoken');

const { generateError } = require('../helpers');

const isAuth = async (req, res, next) => {
    try {
        const { authorization } = req.headers;

        if (!authorization) {
            generateError('Falta la cabecera de autenticación', 400);
        }

        let tokenInfo;

        try {
            tokenInfo = jwt.verify(authorization, process.env.SECRET);
        } catch {
            generateError('Token incorrecto', 401);
        }

        req.user = tokenInfo;

        next();
    } catch (err) {
        next(err);
    }
};

module.exports = isAuth;
