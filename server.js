'use strict';

require('dotenv').config();
const express = require('express');
const fileUpload = require('express-fileupload');
const morgan = require('morgan');
const path = require('path');
const cors = require('cors');

const {
    newUser,
    loginUser,
    getUser,
    modifyUserInfo,
} = require('./controllers/users/index');
const {
    newFolder,
    newFile,
    getFolder,
    deleteFolder,
    deleteFile,
    listData,
    downloadFile,
} = require('./controllers/folders/index');
const isAuth = require('./middlewares/isAuth');
const { createStaticDir } = require('./helpers');
const folderExists = require('./middlewares/folderExists');

const app = express();

app.use(cors());

app.use(express.json());

const staticDirPath = path.join(__dirname, process.env.UPLOADS_DIR);
app.use(express.static(staticDirPath));

createStaticDir(staticDirPath);

app.use(fileUpload());

app.use(morgan('dev'));

/**
 * ###########
 * ## USERS ##
 * ###########
 */

//POST - /users - Resgistro en el sistema - sin token
app.post('/users', newUser);

//POST - /login - Inicio de sesión - sin token
app.post('/login', loginUser);

//GET - /users - Información del usuario - token
app.get('/users', isAuth, getUser);

//PUT - /users - Modifica información del usuario - token
app.put('/users', isAuth, modifyUserInfo);

/**
 * #############
 * ## FOLDERS ##
 * #############
 */

// POST - /folders - Crear carpeta - token
app.post('/folders', isAuth, newFolder);

// POST - /folders/:idFolder/files - Crear archivo - token
app.post('/folders/:idFolder/files', isAuth, newFile);

// GET - /folders - Retorna todas las carpetas y archivos de la raiz del usuario - token
app.get('/folders', isAuth, listData);

// GET - /folders/:idFolder - Retorna una carpeta concreta con sus archivos - token
app.get('/folders/:idFolder', isAuth, folderExists, getFolder);

//GET - /download/:filename - Descarga de archivo - token
app.get('/download/:filename', isAuth, downloadFile);

// DELETE - /folders/:idFolder - Borrado de carpeta - token
app.delete('/folders/:idFolder', isAuth, folderExists, deleteFolder);

// DELETE - /folders/:idFolder/files/:idFile - Borrado de archivo - token
app.delete(
    '/folders/:idFolder/files/:idFile',
    isAuth,
    folderExists,
    deleteFile
);

/**
 * ########################
 * ## GESTIÓN DE ERRORES ##
 * ########################
 */

// Middleware de error.
app.use((err, req, res, next) => {
    console.error(err);

    res.status(err.statusCode || 500).send({
        status: 'error',
        message: err.message,
    });
});

// Middleware de ruta no encontrada.
app.use((req, res) => {
    res.status(404).send({
        status: 'error',
        message: 'Ruta no encontrada',
    });
});

/**
 * ##########################
 * ## ESCUCHA DEL SERVIDOR ##
 * ##########################
 */

app.listen(process.env.PORT, () => {
    console.log(`Server listening at http://localhost:${process.env.PORT}`);
});
