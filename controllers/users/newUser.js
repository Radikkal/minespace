'use strict';

const { generateError } = require('../../helpers');
const insertUserQuery = require('../../bbdd/queries/users/insertUserQuery');

const newUser = async (req, res, next) => {
    try {
        const { name, email, password } = req.body;

        if (!name || !email || !password) {
            generateError('Faltan campos', 400);
        }

        await insertUserQuery(name, email, password);

        res.send({
            status: 'ok',
            message: 'Usuario creado',
        });
    } catch (err) {
        next(err);
    }
};

module.exports = newUser;
