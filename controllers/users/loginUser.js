'use strict';
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { generateError } = require('../../helpers');
const selectUserByEmailQuery = require('../../bbdd/queries/users/selectUserByEmailQuery');

const loginUser = async (req, res, next) => {
    try {
        const { email, password } = req.body;

        if (!email || !password) {
            generateError('Faltan campos', 400);
        }

        const user = await selectUserByEmailQuery(email);

        const validPass = await bcrypt.compare(password, user.password);

        if (!validPass) {
            generateError('Contraseña incorrecta', 401);
        }

        const tokenInfo = {
            id: user.id,
            role: user.role,
        };

        const token = jwt.sign(tokenInfo, process.env.SECRET, {
            expiresIn: '1d',
        });

        res.send({
            status: 'ok',
            data: {
                token,
            },
        });
    } catch (err) {
        next(err);
    }
};

module.exports = loginUser;
