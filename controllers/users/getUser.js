'use strict';

const selectUserByIdQuery = require('../../bbdd/queries/users/selectUserByIdQuery');

const getUser = async (req, res, next) => {
    try {
        const infoUser = await selectUserByIdQuery(req.user.id);

        res.send({
            status: 'ok',
            data: {
                infoUser,
            },
        });
    } catch (err) {
        next(err);
    }
};

module.exports = getUser;
