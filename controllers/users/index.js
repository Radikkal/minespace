'use strict';

const newUser = require('./newUser');
const loginUser = require('./loginUser');
const getUser = require('./getUser');
const modifyUserInfo = require('./modifyUserInfo');

module.exports = { newUser, loginUser, getUser, modifyUserInfo };
