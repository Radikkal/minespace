'use strict';

const { generateError } = require('../../helpers');
const selectUserByIdQuery = require('../../bbdd/queries/users/selectUserByIdQuery');
const updateUserQuery = require('../../bbdd/queries/users/updateUserQuery');
const isEmailQuery = require('../../bbdd/queries/users/isEmailQuery');
const bcrypt = require('bcrypt');

const modifyUserInfo = async (req, res, next) => {
    try {
        let { name, email, password, currentPassword: oldPassword } = req.body;
        let validPass = false;

        if (!name && !email && !password && !oldPassword) {
            generateError('Faltan campos', 400);
        }

        const infoUser = await selectUserByIdQuery(req.user.id);

        name = name || infoUser.name;
        email = email || infoUser.email;
        password = password || infoUser.password;

        if (email !== infoUser.email) {
            const isEmail = await isEmailQuery(email);
        }

        if (oldPassword) {
            validPass = await bcrypt.compare(oldPassword, infoUser.password);

            if (validPass) {
                const isSamePass = await bcrypt.compare(
                    password,
                    infoUser.password
                );

                if (!isSamePass) {
                    const hashedPass = await bcrypt.hash(password, 10);

                    password = hashedPass;
                } else {
                    generateError(
                        'La nueva contraseña no puede ser igual a la anterior',
                        401
                    );
                }
            } else {
                generateError('Contraseña actual incorrecta', 401);
            }
        }
        await updateUserQuery(name, email, password, req.user.id);

        res.send({
            status: 'ok',
            data: {
                user: {
                    name,
                    email,
                },
            },
        });
    } catch (err) {
        next(err);
    }
};

module.exports = modifyUserInfo;
