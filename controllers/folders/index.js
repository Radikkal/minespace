'use strict';

const newFolder = require('./newFolder');
const newFile = require('./newFile');
const getFolder = require('./getFolder');
const deleteFolder = require('./deleteFolder');
const deleteFile = require('./deleteFile');
const listData = require('./listData');
const downloadFile = require('./downloadFile');

module.exports = {
    newFolder,
    newFile,
    getFolder,
    deleteFolder,
    deleteFile,
    listData,
    downloadFile,
};
