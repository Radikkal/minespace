'use strict';

const selectAllDataQuery = require('../../bbdd/queries/folders/selectAllDataQuery');

const listData = async (req, res, next) => {
    try {
        const idUser = req.user.id;

        const allData = await selectAllDataQuery(idUser);

        res.send({
            status: 'ok',
            data: {
                allData,
            },
        });
    } catch (err) {
        next(err);
    }
};

module.exports = listData;
