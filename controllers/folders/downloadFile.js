const fs = require('fs/promises');
const path = require('path');
const { generateError } = require('../../helpers');

const downloadFile = async (req, res, next) => {
    try {
        const { filename } = req.params;

        const staticDirPath = path.join(
            __dirname,
            '../../',
            process.env.UPLOADS_DIR
        );

        const filePath = path.join(staticDirPath, filename);

        //comprobamos que el fichero exista
        await checkIfPathExists(filePath);

        res.download(filePath);
    } catch (error) {
        next(error);
    }
};

async function checkIfPathExists(path) {
    try {
        await fs.access(path);
    } catch (error) {
        throw generateError('El fichero no existe', 404);
    }
}

module.exports = downloadFile;
