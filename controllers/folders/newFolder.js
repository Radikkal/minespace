'use strict';

const { generateError } = require('../../helpers');
const insertFolderQuery = require('../../bbdd/queries/folders/insertFolderQuery');

const newFolder = async (req, res, next) => {
    try {
        const { name } = req.body;

        if (!name) {
            generateError('Faltan campos', 400);
        }

        const userId = req.user.id;

        await insertFolderQuery(name, userId);

        res.send({
            status: 'ok',
            message: 'Carpeta creada',
        });
    } catch (err) {
        next(err);
    }
};

module.exports = newFolder;
