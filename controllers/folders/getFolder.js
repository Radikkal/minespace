'use strict';
const selectFolderQuery = require('../../bbdd/queries/folders/selectFolderQuery');

const getFolder = async (req, res, next) => {
    try {
        const { idFolder } = req.params;

        const folder = await selectFolderQuery(idFolder);

        res.send({
            status: 'ok',
            data: {
                folder,
            },
        });
    } catch (err) {
        next(err);
    }
};

module.exports = getFolder;
