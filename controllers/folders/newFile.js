'use strict';

const { generateError } = require('../../helpers');
const insertFileQuery = require('../../bbdd/queries/folders/insertFileQuery');
const checkIfStaticFolderQuery = require('../../bbdd/queries/folders/checkIfStaticFolderQuery');
const folderUserIdQuery = require('../../bbdd/queries/folders/folderUserIdQuery');

const newFile = async (req, res, next) => {
    try {
        const { idFolder } = req.params;
        const { id: idUser } = req.user;

        const [folderUserId] = await folderUserIdQuery(idFolder);

        const isStatic = await checkIfStaticFolderQuery(idFolder);

        if (!isStatic) {
            if (req.user.id !== folderUserId[0].user_id) {
                generateError('No tienes suficientes permisos', 401);
            }
        }

        if (!(req.files && Object.keys(req.files).length > 0)) {
            generateError('Faltan campos', 400);
        }

        const files = Object.values(req.files);

        await insertFileQuery(idUser, idFolder, files);

        res.send({
            status: 'ok',
            message: 'Fichero subido',
        });
    } catch (err) {
        next(err);
    }
};

module.exports = newFile;
