'use strict';

const deleteFilesQuery = require('../../bbdd/queries/folders/deleteFilesQuery');

const deleteFile = async (req, res, next) => {
    try {
        const { idFile } = req.params;

        await deleteFilesQuery('', idFile);

        res.send({
            status: 'ok',
            message: 'Archivo borrado',
        });
    } catch (err) {
        next(err);
    }
};

module.exports = deleteFile;
