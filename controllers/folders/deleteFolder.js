'use strict';

const selectFolderFilesQuery = require('../../bbdd/queries/folders/selectFolderFilesQuery');
const folderUserIdQuery = require('../../bbdd/queries/folders/folderUserIdQuery');
const deleteFilesQuery = require('../../bbdd/queries/folders/deleteFilesQuery');
const deleteFolderQuery = require('../../bbdd/queries/folders/deleteFolderQuery');
const { generateError, deleteFiles } = require('../../helpers');

const deleteFolder = async (req, res, next) => {
    try {
        const { idFolder } = req.params;

        const [folderUserId] = await folderUserIdQuery(idFolder);

        if (req.user.id !== folderUserId[0].user_id) {
            generateError('No tienes suficientes permisos', 401);
        }

        const folderInfo = await selectFolderFilesQuery(idFolder);

        if (folderInfo.length > 0) {
            await deleteFiles(folderInfo);

            await deleteFilesQuery(folderInfo);
        }

        await deleteFolderQuery(idFolder);

        res.send({
            status: 'ok',
            message: 'Carpeta borrada',
        });
    } catch (err) {
        next(err);
    }
};

module.exports = deleteFolder;
