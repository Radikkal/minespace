MineSpace API (Backend)

API que permite a los usuarios registrarse y acceder a un espacio personal donde subir ficheros, creada por Alejandro López, Xurxo Sobrino y Estefanía Gómez.

Frontend de esta API:
https://gitlab.com/stefaniagomez1/minespace-react

Instalar

1️⃣ Crear una base de datos vacía en una instancia de MySQL local.
2️⃣ Ejecutar npm i para instalar dependencias.
3️⃣ Guardar el archivo .env.example como .env y cubrir los datos necesarios.
4️⃣ Ejecutar npm run initDB para crear las tablas necesarias en la base de datos anteriormente creada.
5️⃣ Ejecutar npm run dev o npm start para lanzar el servidor.

Entidades

    Users:
        - id
        - name
        - email
        - password
        - role
        - creationDate

    Folders:
        - id
        - name
        - creationDate
        - user_id

    Files:
        - id
        - name
        - creationDate
        - folder_id
        - user_id

Endpoints

Users

    POST - /users - Resgistro en el sistema - sin token
    POST - /login - Inicio de sesión - sin token
    GET - /users - Información del usuario - token
    PUT - /users - Modifica información del usuario - token

Folders

    POST - /folders - Crear carpeta - token
    POST - /folders/:idFolder/files - Crear archivo - token
    GET - /folders - Retorna todas las carpetas y archivos de la raiz del usuario. - token
    GET - /folders/:idFolder - Retorna una carpeta concreta con sus archivos - token
    GET - /download/:filename - Descarga de archivo - token
    DELETE - /folders/:idFolder - Borrado de carpeta - token
    DELETE - /folders/:idFolder/files/:idFile - Borrado de archivo - token
