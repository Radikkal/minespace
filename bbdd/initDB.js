'use strict';

require('dotenv').config();
const { v4: uuid } = require('uuid');
const bcrypt = require('bcrypt');

const getDB = require('./getConnection');

async function main() {
    let connection;
    try {
        connection = await getDB();

        console.log('Borrando tablas existentes...');

        await connection.query(`DROP TABLE IF EXISTS files`);
        await connection.query(`DROP TABLE IF EXISTS folders`);
        await connection.query(`DROP TABLE IF EXISTS users`);

        console.log('Creando tablas...');

        await connection.query(`
            CREATE TABLE users(
                id CHAR(36) NOT NULL PRIMARY KEY,
                name VARCHAR(100),
                email VARCHAR(100) UNIQUE NOT NULL,
                password VARCHAR(100) NOT NULL,
                role ENUM("admin","normal") DEFAULT "normal" NOT NULL,
                creationDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
            )
        `);
        await connection.query(`
            CREATE TABLE folders(
                id CHAR(36) NOT NULL PRIMARY KEY,
                name VARCHAR(100) NOT NULL,
                creationDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                user_id CHAR(36) NOT NULL,
                FOREIGN KEY (user_id) REFERENCES users(id)
            )
        `);
        await connection.query(`
            CREATE TABLE files(
                id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
                name VARCHAR(100) NOT NULL,
                creationDate DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                folder_id CHAR(36),
                FOREIGN KEY (folder_id) REFERENCES folders(id),
                user_id CHAR(36) NOT NULL,
                FOREIGN KEY (user_id) REFERENCES users(id)
            )
        `);

        const adminId = uuid();

        const staticFolderId = uuid();

        const adminPwd = await bcrypt.hash(process.env.USER_ADMIN_PWD, 10);

        await connection.query(
            `
            INSERT INTO users(id,name,email,password,role) VALUES (?,"${process.env.USER_ADMIN_NAME}","${process.env.USER_ADMIN_EMAIL}","${adminPwd}",?)
            `,
            [adminId, 'admin']
        );

        await connection.query(
            `
            INSERT INTO folders(id,name,user_id) VALUES (?,"${process.env.UPLOADS_DIR}",?)
            `,
            [staticFolderId, adminId]
        );

        console.log('¡Tablas creadas!');
    } catch (err) {
        console.error(err);
    } finally {
        if (connection) connection.release();

        process.exit();
    }
}

main();
