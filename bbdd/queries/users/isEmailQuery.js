'use strict';

const { generateError } = require('../../../helpers');
const getDB = require('../../getConnection');

const isEmailQuery = async (email) => {
    let connection;

    try {
        connection = await getDB();

        const [emailExists] = await connection.query(
            `
            SELECT email FROM users WHERE email = ?
            `,
            [email]
        );
        if (emailExists.length > 0) {
            generateError('Ya existe un usuario con ese email', 409);
        }
    } finally {
        if (connection) connection.release();
    }
};

module.exports = isEmailQuery;
