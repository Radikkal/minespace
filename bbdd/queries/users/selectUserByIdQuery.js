'use strict';

const { generateError } = require('../../../helpers');
const getDB = require('../../getConnection');

const selectUserByIdQuery = async (userId) => {
    let connection;

    try {
        connection = await getDB();

        const [userInfo] = await connection.query(
            `
            SELECT name, email, creationDate, password FROM users WHERE id = ?
            `,
            [userId]
        );

        if (userInfo.length < 1) {
            generateError('Usuario no encontrado', 404);
        }

        return userInfo[0];
    } finally {
        if (connection) connection.release();
    }
};

module.exports = selectUserByIdQuery;
