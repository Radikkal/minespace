'use strict';

const getDB = require('../../getConnection');
const { generateError } = require('../../../helpers');

const { v4: uuid } = require('uuid');
const bcrypt = require('bcrypt');

const insertUserQuery = async (name, email, password) => {
    let connection;

    try {
        connection = await getDB();

        const [users] = await connection.query(
            `
            SELECT id FROM users WHERE email = ?
            `,
            [email]
        );

        if (users.length > 0) {
            generateError('Ya existe un usuario con ese email', 409);
        }

        const hashedPass = await bcrypt.hash(password, 10);

        let id = uuid();

        // Comprobamos que el uuid no se repita
        let [repeatedId] = await connection.query(
            `
            SELECT id FROM users WHERE id=?
            `,
            [id]
        );

        while (repeatedId.length === 1) {
            id = uuid();

            [repeatedId] = await connection.query(
                `
                SELECT id FROM users WHERE id=?
                `,
                [id]
            );
        }

        await connection.query(
            `
            INSERT INTO users (id, name, email, password) VALUES (?, ?, ?, ?)
            `,
            [id, name, email, hashedPass]
        );
    } finally {
        if (connection) connection.release();
    }
};

module.exports = insertUserQuery;
