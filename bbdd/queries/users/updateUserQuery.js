'use strict';

const getDB = require('../../getConnection');

const updateUserQuery = async (name, email, password, id) => {
    let connection;
    try {
        connection = await getDB();

        await connection.query(
            `
            UPDATE users SET name=?, email=?, password=? WHERE id=?
        `,
            [name, email, password, id]
        );
    } finally {
        if (connection) connection.release();
    }
};

module.exports = updateUserQuery;
