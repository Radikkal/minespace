'use strict';

const getDB = require('../../getConnection');

const deleteFolderQuery = async (idFolder) => {
    let connection;

    try {
        connection = await getDB();

        await connection.query(
            `
            DELETE FROM folders WHERE id = ?
            `,
            [idFolder]
        );
    } finally {
        if (connection) connection.release();
    }
};

module.exports = deleteFolderQuery;
