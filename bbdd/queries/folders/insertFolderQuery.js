'use strict';

const getDB = require('../../getConnection');

const { v4: uuid } = require('uuid');

const insertFolderQuery = async (name, userId) => {
    let connection;
    try {
        connection = await getDB();

        let id = uuid();

        let [repeatedId] = await connection.query(
            `
            SELECT id FROM folders WHERE id=?
            `,
            [id]
        );

        while (repeatedId.length === 1) {
            id = uuid();

            [repeatedId] = await connection.query(
                `
            SELECT id FROM folders WHERE id=?
            `,
                [id]
            );
        }

        await connection.query(
            `
            INSERT INTO folders(id,name,user_id) VALUES (?,?,?)
            `,
            [id, name, userId]
        );

        return { id, name };
    } finally {
        if (connection) connection.release();
    }
};

module.exports = insertFolderQuery;
