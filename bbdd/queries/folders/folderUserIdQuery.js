'use strict';

const getDB = require('../../getConnection');

const folderUserIdQuery = async (idFolder) => {
    let connection;

    try {
        connection = await getDB();

        const userId = await connection.query(
            `
            SELECT user_id FROM folders WHERE id = ?
            `,
            [idFolder]
        );

        return userId;
    } finally {
        if (connection) connection.release();
    }
};

module.exports = folderUserIdQuery;
