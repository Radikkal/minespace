'use strict';

const getDB = require('../../getConnection');
const { savePhoto, saveFile } = require('../../../helpers');

const path = require('path');

const insertFileQuery = async (idUser, idFolder, files) => {
    let connection;

    try {
        connection = await getDB();

        const fileNames = [];

        for (const fileData of files) {
            const isImage = fileData.mimetype.split('/');

            if (isImage[0] === 'image') {
                const imgName = await savePhoto(fileData);
                fileNames.push(imgName);
            } else {
                const fileName = await saveFile(fileData);
                fileNames.push(fileName);
            }
        }

        for (const file of fileNames) {
            await connection.query(
                `
                    INSERT INTO files(name, folder_id, user_id) VALUES (?, ?, ?)
                `,
                [file, idFolder, idUser]
            );
        }
    } finally {
        if (connection) connection.release();
    }
};

module.exports = insertFileQuery;
