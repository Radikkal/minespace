'use strict';

const getDB = require('../../getConnection');

const selectAllDataQuery = async (idUser) => {
    let connection;
    try {
        connection = await getDB();

        const allData = [];

        const [staticFolderId] = await connection.query(`
            SELECT id FROM folders ORDER BY creationDate LIMIT 1
        `);

        allData.push({
            staticFolderId,
        });

        let [foldersData] = await connection.query(
            `
            SELECT id, name FROM folders WHERE user_id = ?
            `,
            [idUser]
        );

        for (const folder of foldersData) {
            allData.push({
                folderName: folder.name,
                folderId: folder.id,
            });
        }

        const [filesData] = await connection.query(
            `
            SELECT id, name FROM files WHERE user_id = ? AND folder_id = ?
            `,
            [idUser, staticFolderId[0].id]
        );

        for (const file of filesData) {
            allData.push({
                fileName: file.name,
                fileId: file.id,
                staticFolderId: staticFolderId[0].id,
            });
        }

        return allData;
    } finally {
        if (connection) connection.release();
    }
};

module.exports = selectAllDataQuery;
