'use strict';

const getDB = require('../../getConnection');

const deleteFilesQuery = async (fileNames = '', fileId = '') => {
    let connection;

    try {
        connection = await getDB();

        let response;
        let value;
        let data;

        if (fileNames) {
            response = `name = ?`;
            data = fileNames;
        } else {
            response = `id = ?`;
            data = [fileId];
        }
        for (const file of data) {
            if (fileNames) {
                value = file.name;
            } else {
                value = file;
            }

            await connection.query(
                `
                DELETE FROM files WHERE ${response}
                `,
                [value]
            );
        }
    } finally {
        if (connection) connection.release();
    }
};

module.exports = deleteFilesQuery;
