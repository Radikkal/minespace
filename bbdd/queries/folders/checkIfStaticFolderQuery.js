'use strict';

const getDB = require('../../getConnection');

const checkIfStaticFolderQuery = async (idFolder) => {
    let connection;

    try {
        connection = await getDB();

        const [isStatic] = await connection.query(
            `
            SELECT * FROM folders WHERE name = ? AND id = ?
            `,
            [process.env.UPLOADS_DIR, idFolder]
        );

        if (isStatic.length < 1) {
            return false;
        }
        return true;
    } finally {
        if (connection) connection.release();
    }
};

module.exports = checkIfStaticFolderQuery;
