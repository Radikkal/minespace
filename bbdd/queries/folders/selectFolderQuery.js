'use strict';

const getDB = require('../../getConnection');

const selectFolderQuery = async (idFolder) => {
    let connection;

    try {
        connection = await getDB();

        const [folderFiles] = await connection.query(
            `
            SELECT fi.id, fi.name 
            FROM folders f
            INNER JOIN files fi ON f.id = fi.folder_id
            WHERE folder_id = ?;
            `,
            [idFolder]
        );

        return folderFiles;
    } finally {
        if (connection) connection.release();
    }
};
module.exports = selectFolderQuery;
