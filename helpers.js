'use strict';

const fs = require('fs/promises');
const path = require('path');
const sharp = require('sharp');
const { v4: uuid } = require('uuid');

const generateError = (msg, code) => {
    const err = new Error(msg);
    err.statusCode = code;
    throw err;
};

// Función que crea el directorio de ficheros estáticos si no existe.
const createStaticDir = async (path) => {
    try {
        await fs.access(path);
    } catch {
        await fs.mkdir(path);
    }
};

const savePhoto = async (img) => {
    createStaticDir(path.join(__dirname, process.env.UPLOADS_DIR));

    const sharpImg = sharp(img.data);

    const imgName = `${uuid()}_${img.name}`;

    await sharpImg.toFile(
        path.join(__dirname, process.env.UPLOADS_DIR, imgName)
    );

    return imgName;
};

const saveFile = async (file) => {
    const uploadPath = path.join(__dirname, process.env.UPLOADS_DIR);

    createStaticDir(uploadPath);

    const fileName = `${uuid()}_${file.name}`;

    const filePath = path.join(uploadPath, fileName);

    await file.mv(filePath);

    return fileName;
};

const deleteFiles = async (files) => {
    for (const file of files) {
        const filePath = path.join(
            __dirname,
            process.env.UPLOADS_DIR,
            file.name
        );

        await fs.unlink(filePath);
    }
};

module.exports = {
    generateError,
    createStaticDir,
    savePhoto,
    saveFile,
    deleteFiles,
};
